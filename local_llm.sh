#! /bin/bash
set -eo pipefail

# example usage
# setup: my_llm.sh spinup <model>
# use: my_llm.sh talk <model> <text...>
# open webui interface: local_llm.sh webui <?wsl for if needed to open in host windows>

ollama_container="ollama"
webui_container="open-webui"
model="gemma:2b"

# command processing
if [ $# -ne 0 ]; then
    if [ $1 = "spinup" ]; then
        # check if instance if active
        if [ $(docker stats --no-stream | grep -c " $ollama_container ") -ne 0 ]; then
            echo "'$ollama_container' is already runnning"
        else
            echo "Spinning up LLM container as '$ollama_container'..."
            docker run -d -v ollama:/root/.ollama -p 11434:11434 --name $ollama_container ollama/ollama
        fi

        # pull custom model if specified and build
        if [ $# -eq 2 ]; then
            model=$2
        fi

        docker exec $ollama_container ollama pull $model
    elif [ $1 = "talk" ]; then
        if [ $# -lt 3 ]; then
            echo "Please use arguments: talk <model> <text...>"
            exit 1
        fi

        # pull custom model if specified and build
        model=$2

        docker exec $ollama_container ollama run $model ${@:2:$#}
    elif [ $1 = "webui" ]; then
        # check if instance if active
        if [ $(docker stats --no-stream | grep -c " $webui_container ") -ne 0 ]; then
            echo "'open-webui' is already runnning"
        else
            echo "Spinning up local open-webui container as 'open-webui'..."
            docker run -d -p 3000:8080 --add-host=host.docker.internal:host-gateway -v open-webui:/app/backend/data --name $webui_container ghcr.io/open-webui/open-webui:main
        fi

        # allow option to open from wsl 
        if [ $# -eq 2 ]; then
            if [ $2 = "wsl" ]; then
                wslview http://localhost:3000
            else
                echo "[ERROR] '$2' is not a vald alternative "
                exit 1
            fi
        else
            xdg-open http://localhost:3000
        fi
    else
        echo "[ERROR] '$1' is not a valid command"
        exit 1
    fi
else
    echo "[ERROR] Please specify a command"
    exit 1
fi

exit 0